# Fantom

## Core & Periphery contracts

{% hint style="warning" %}
_Pool_, _PoolConfigurator_, _Incentives_ and _Treasury_ addresses mentioned below are of Upgradeable Proxy contract. While interacting please use _abi_ of implementation contracts or generate abi from the github source code linked.&#x20;
{% endhint %}

| Contract                      | Github                                                                                                                                | Address                                    |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |
| ACLManager                    | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/protocol/configuration/ACLManager.sol)                            | 0xa72636CbcAa8F5FF95B2cc47F3CDEe83F3294a0B |
| Pool                          | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/protocol/pool/Pool.sol)                                           | 0x794a61358D6845594F94dc1DB02A252b5b4814aD |
| PoolConfigurator              | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/protocol/pool/PoolConfigurator.sol)                               | 0x8145eddDf43f50276641b55bd3AD95944510021E |
| Incentives                    | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/rewards/RewardsController.sol)                               | 0x929EC64c34a17401F460460D4B9390518E5B473e |
| PullRewardsTransferStrategy   | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/rewards/transfer-strategies/PullRewardsTransferStrategy.sol) | 0x84d85013035BbFF5DD106CA7828c8781af331b84 |
| PoolAddressesProvider         | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/protocol/configuration/PoolAddressesProvider.sol)                 | 0xa97684ead0e402dC232d5A977953DF7ECBaB3CDb |
| PoolAddressesProviderRegistry | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/protocol/configuration/PoolAddressesProviderRegistry.sol)         | 0x770ef9f4fe897e59daCc474EF11238303F9552b6 |
| PoolDataProvider              | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/misc/AaveProtocolDataProvider.sol)                                | 0x69FA688f1Dc47d4B5d8029D5a35FB7a548310654 |
| UiIncentiveDataProviderV3     | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/misc/UiIncentiveDataProviderV3.sol)                          | 0xbA14c06011f4AF5970cFDe4364ba6320E190BD4B |
| UiPoolDataProviderV3          | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/misc/UiPoolDataProviderV3.sol)                               | 0x1CCbfeC508da8D5242D5C1b368694Ab0066b39f1 |
| WETHGateway                   | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/misc/WETHGateway.sol)                                        | 0x17d013C19FE25cf4D911CE85eD5f40FE8880F46f |
| WalletBalanceProvider         | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/misc/WalletBalanceProvider.sol)                              | 0xBc790382B3686abffE4be14A030A96aC6154023a |
| AaveOracle                    | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/misc/AaveOracle.sol)                                              | 0xfd6f3c1845604C8AE6c6E402ad17fb9885160754 |
| Treasury                      | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/treasury/Collector.sol)                                      | 0xfd6f3c1845604C8AE6c6E402ad17fb9885160754 |
| TreasuryController            | [Github](https://github.com/aave/aave-v3-periphery/blob/master/contracts/treasury/CollectorController.sol)                            | 0xc0F0cFBbd0382BcE3B93234E4BFb31b2aaBE36aD |
| FallbackOracle                | [Github](https://github.com/aave/aave-v3-core/blob/master/contracts/mocks/oracle/PriceOracle.sol)                                     | 0x8aA68Ca9e25aAb6f9f41bF341d12Ab407AE099E2 |

## Tokens

| Token                               | Address                                    |
| ----------------------------------- | ------------------------------------------ |
| DAI-AToken-Fantom (aDAI)            | 0x82E64f49Ed5EC1bC6e43DAD4FC8Af9bb3A2312EE |
| DAI-StableDebtToken-Fantom (sDAI)   | 0xd94112B5B62d53C9402e7A60289c6810dEF1dC9B |
| DAI-VariableDebtToken-Fantom (vDAI) | 0x8619d80FB0141ba7F184CbF22fd724116D9f7ffC |
| LINK-AToken-Fantom (aLINK)          | 0x191c10Aa4AF7C30e871E70C95dB0E4eb77237530 |
| LINK-StableDebtToken-Fantom         | 0x89D976629b7055ff1ca02b927BA3e020F22A44e4 |
| LINK-VariableDebtToken-Fantom       | 0x953A573793604aF8d41F306FEb8274190dB4aE0e |
| USDC-AToken-Fantom                  | 0x625E7708f30cA75bfd92586e17077590C60eb4cD |
| USDC-StableDebtToken-Fantom         | 0x307ffe186F84a3bc2613D1eA417A5737D69A7007 |
| USDC-VariableDebtToken-Fantom       | 0xFCCf3cAbbe80101232d343252614b6A3eE81C989 |
| WBTC-AToken-Fantom                  | 0x078f358208685046a11C85e8ad32895DED33A249 |
| WBTC-StableDebtToken-Fantom         | 0x633b207Dd676331c413D4C013a6294B0FE47cD0e |
| WBTC-VariableDebtToken-Fantom       | 0x92b42c66840C7AD907b4BF74879FF3eF7c529473 |
| WETH-AToken-Fantom                  | 0xe50fA9b3c56FfB159cB0FCA61F5c9D750e8128c8 |
| WETH-StableDebtToken-Fantom         | 0xD8Ad37849950903571df17049516a5CD4cbE55F6 |
| WETH-VariableDebtToken-Fantom       | 0x0c84331e39d6658Cd6e6b9ba04736cC4c4734351 |
| USDT-AToken-Fantom                  | 0x6ab707Aca953eDAeFBc4fD23bA73294241490620 |
| USDT-StableDebtToken-Fantom         | 0x70eFfc565DB6EEf7B927610155602d31b670e802 |
| USDT-VariableDebtToken-Fantom       | 0xfb00AC187a8Eb5AFAE4eACE434F493Eb62672df7 |
| AAVE-AToken-Fantom                  | 0xf329e36C7bF6E5E86ce2150875a84Ce77f477375 |
| AAVE-StableDebtToken-Fantom         | 0xfAeF6A702D15428E588d4C0614AEFb4348D83D48 |
| AAVE-VariableDebtToken-Fantom       | 0xE80761Ea617F66F96274eA5e8c37f03960ecC679 |
| WFTM-AToken-Fantom                  | 0x6d80113e533a2C0fe82EaBD35f1875DcEA89Ea97 |
| WFTM-StableDebtToken-Fantom         | 0xF15F26710c827DDe8ACBA678682F3Ce24f2Fb56E |
| WFTMVariableDebtToken-Fantom        | 0x4a1c3aD6Ed28a636ee1751C69071f6be75DEb8B8 |
| CRV-AToken-Fantom                   | 0x513c7E3a9c69cA3e22550eF58AC1C0088e918FFf |
| CRV-StableDebtToken-Fantom          | 0x08Cb71192985E936C7Cd166A8b268035e400c3c3 |
| CRV-VariableDebtToken-Fantom        | 0x77CA01483f379E58174739308945f044e1a764dc |
| SUSHI-AToken-Fantom                 | 0xc45A479877e1e9Dfe9FcD4056c699575a1045dAA |
| SUSHI-StableDebtToken-Fantom        | 0x78246294a4c6fBf614Ed73CcC9F8b875ca8eE841 |
| SUSHI-VariableDebtToken-Fantom      | 0x34e2eD44EF7466D5f9E0b782B5c08b57475e7907 |

